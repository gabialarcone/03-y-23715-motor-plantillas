const sqlite3 = require('sqlite3').verbose();
const path = require('path');
const dbPath = path.resolve(__dirname, 'integrantes.sqlite');
const db = new sqlite3.Database(dbPath);


db.serialize(() => {
    db.run('SELECT 1', [], (err) => {
        if (err) {
            console.error('Error al conectar con la base de datos:', err);
        } else {
            console.log('Conexión exitosa con la base de datos.');
            db.run("select * from integrante");
        }
    });
});

async function getAll(query, parametros){
    return new Promise((resolve, reject)=>{
        db.all(query, parametros, (error, rows)=>{
            if (error) {
                reject(error);
            } else{
                resolve(rows);
            }
        });
    });
}
module.exports = {db, getAll};
