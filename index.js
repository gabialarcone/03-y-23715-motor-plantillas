//importar express
const express = require("express");
const hbs = require("hbs");
const app = express();


// importamos el archivo de rutas
const router = require("./routes/public");
app.use("/", router);

app.use(express.static(__dirname + "/public"));
app.set('view engine', 'hbs');
app.set('views', __dirname + "/views");

hbs.registerPartials(__dirname + "/views/partials");

app.use((req, res, next) => {
    res.status(404).render('aviso'); // Renderizar el archivo mensaje.hbs desde la carpeta partials
});

// Iniciar el servidor en el puerto 3000

const puerto = process.env.PORT
app.listen(puerto, () => {
    console.log("El servidor se está ejecutando en http://localhost:" + puerto);
});

